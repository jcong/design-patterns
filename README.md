<Head first of Design Patterns>

设计原则：

- 封装变化

- 针对接口编程

- 多用组合少用继承

- 为交互对象之间的松耦合设计而努力

- 开闭原则：对扩展开放，对修改关闭

设计模式：

- [策略模式](01_Strategy/)
- [观察者模式](02_Observer/)
- [装饰者模式](03_Decorator/)
- [工厂模式](04_Factory)