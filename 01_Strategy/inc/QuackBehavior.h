#ifndef __QUACK_BEHAVIOR_H__
#define __QUACK_BEHAVIOR_H__

// interface
class QuackBehavior {
public:
    virtual void quack() = 0;
};

#endif // __QUACK_BEHAVIOR_H__