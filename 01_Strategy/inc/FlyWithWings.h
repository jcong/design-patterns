#ifndef __FLY_WITH_WINGS_H__
#define __FLY_WITH_WINGS_H__

#include "FlyBehavior.h"

class FlyWithWings : public FlyBehavior {
public:
    virtual void fly() final;
};

#endif // __FLY_WITH_WINGS_H__