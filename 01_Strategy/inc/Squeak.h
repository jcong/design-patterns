#ifndef __SQUEAK_H__
#define __SQUEAK_H__

#include "QuackBehavior.h"

class Squeak : public QuackBehavior {
public:
    virtual void quack() final;
};

#endif // _SQUEAK_H__