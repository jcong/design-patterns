#ifndef __DUCK_H__
#define __DUCK_H__

#include "FlyBehavior.h"
#include "QuackBehavior.h"

class Duck {
public:
    virtual void quack();
    virtual void swim();
    virtual void display();

    // new
    Duck() = default;
    virtual ~Duck() = default;

    virtual void performQuack();
    virtual void performFly();
    virtual void setFlayBehavior(FlyBehavior *);
    virtual void setQuackBehavior(QuackBehavior *);

    FlyBehavior *pImplFly;
    QuackBehavior *pImplQuack;
};

#endif // __DUCK_H__