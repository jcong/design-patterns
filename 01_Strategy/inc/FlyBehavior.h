#ifndef __FLY_BEHAVIOR_H__
#define __FLY_BEHAVIOR_H__

// interface
class FlyBehavior {
public:
    virtual void fly() = 0;
};

#endif // __FLY_BEHAVIOR_H__