#ifndef __MUTE_QUACK_H__
#define __MUTE_QUACK_H__

#include "QuackBehavior.h"

class MuteQuack : public QuackBehavior {
public:
    virtual void quack() final;
};

#endif // __MUTE_QUACK_H__