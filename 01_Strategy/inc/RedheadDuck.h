#ifndef __REDHEAD_DUCK_H__
#define __REDHEAD_DUCK_H__

#include "Duck.h"

class ReadheadDuck : public Duck {
public:
    ReadheadDuck();
    ~ReadheadDuck();
    virtual void display() final;
};

#endif // __REDHEAD_DUCK_H__