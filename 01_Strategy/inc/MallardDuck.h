#ifndef __MALLARD_DUCK_H__
#define __MALLARD_DUCK_H__

#include "Duck.h"

class MallardDuck : public Duck {
public:
    MallardDuck();
    ~MallardDuck();
    virtual void display() final;
};

#endif // __MALLARD_DUCK_H__