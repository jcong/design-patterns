#ifndef __QUACK_H__
#define __QUACK_H__

#include "QuackBehavior.h"

class Quack : public QuackBehavior {
public:
    virtual void quack() final;
};

#endif // __QUACK_H__