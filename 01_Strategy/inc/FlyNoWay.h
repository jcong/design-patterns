#ifndef __FLY_NO_WAY_H__
#define __FLY_NO_WAY_H__

#include "FlyBehavior.h"

class FlyNoWay : public FlyBehavior {
public:
    virtual void fly() final;
};

#endif // __FLY_NO_WAY_H__