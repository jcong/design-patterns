#include "Duck.h"
#include "MallardDuck.h"
#include "RedheadDuck.h"

#include <vector>
#include <iostream>

void traverseDuck(std::vector<Duck *> ducks) {
    for (auto *duck : ducks) {
        std::cout << "------ new Duck -------" << std::endl;
        duck->quack();
        duck->swim();
        duck->display();
        std::cout << ">> now use new method <<" << std::endl;
        duck->performFly();
        duck->performQuack();
    }
}

int main() {
    std::vector<Duck *> ducks;
    ducks.emplace_back(new Duck());
    ducks.emplace_back(new MallardDuck());
    ducks.emplace_back(new ReadheadDuck());

    traverseDuck(ducks);

    for (auto *duck : ducks) {
        delete duck;
    }
}