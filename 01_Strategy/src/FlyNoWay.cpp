#include "FlyNoWay.h"

#include <iostream>

void FlyNoWay::fly() {
    std::cout << "Hey! I can't fly..." << std::endl;
}