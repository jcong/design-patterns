#include "Quack.h"

#include <iostream>

void Quack::quack() {
    std::cout << "quack! quack! quack!" << std::endl;
}