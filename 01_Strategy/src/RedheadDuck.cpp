#include "RedheadDuck.h"

#include "FlyNoWay.h"
#include "MuteQuack.h"

#include <iostream>

void ReadheadDuck::display() {
    std::cout << "Hi, I am a readhead duck!!" << std::endl;
}

ReadheadDuck::ReadheadDuck() {
    pImplFly = new FlyNoWay;
    pImplQuack = new MuteQuack;
}

ReadheadDuck::~ReadheadDuck() {
    delete pImplFly;
    delete pImplQuack;
}