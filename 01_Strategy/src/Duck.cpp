#include "Duck.h"

#include <iostream>

void Duck::quack() {
    std::cout << "common duck quack!" << std::endl;
}

void Duck::swim() {
    std::cout << "common duck swim!" << std::endl;
}

void Duck::display() {
    std::cout << "common duck display!" << std::endl;
}

// new
void Duck::performFly() {
    if (pImplFly) {
        pImplFly->fly();
    } else {
        std::cout << "Hey, you haven't set fly behavior" << std::endl;
    }
}

void Duck::performQuack() {
    if (pImplQuack) {
        pImplQuack->quack();
    } else {
        std::cout << "Hey, you haven't set quack behavior" << std::endl;
    }
}

void Duck::setFlayBehavior(FlyBehavior *flyBehavior) {
    pImplFly = flyBehavior;
}

void Duck::setQuackBehavior(QuackBehavior *quackBehavior) {
    pImplQuack = quackBehavior;
}