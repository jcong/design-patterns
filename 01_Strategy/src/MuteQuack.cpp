#include "MuteQuack.h"

#include <iostream>

void MuteQuack::quack() {
    std::cout << "sorry, I can't quack..." << std::endl;
}