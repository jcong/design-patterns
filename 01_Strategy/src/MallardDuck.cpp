#include "MallardDuck.h"

#include "Quack.h"
#include "FlyWithWings.h"

#include <iostream>

void MallardDuck::display() {
    std::cout << "Hi, I am a mallard duck" << std::endl;
}

// new
MallardDuck::MallardDuck() {
    pImplQuack = new Quack;
    pImplFly = new FlyWithWings;
}

MallardDuck::~MallardDuck() {
    delete pImplFly;
    delete pImplQuack;
}