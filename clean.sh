#!/bin/bash

# clear all cache files

function clear() {
    `rm -rf $1"/"CMakeFiles/`
    `rm -rf $1"/"cmake_install.cmake`
    `rm -rf $1"/"Makefile`
    `rm -rf $1"/"CMakeCache.txt`
}

function foreachDir() {
    clear $1
    for e in `ls $1`
    do
        stuff=$1"/"$e
        if [ -d $stuff ]
        then
            foreachDir $stuff
        fi
    done
}

foreachDir $(pwd)