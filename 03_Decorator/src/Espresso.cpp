#include "Espresso.h"

std::string Espresso::getDescription() const {
    return "Espresso";
}

double Espresso::cost() {
    return 1.99;
}