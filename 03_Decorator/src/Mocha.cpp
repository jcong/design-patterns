#include "Mocha.h"

#include <string>

Mocha::Mocha(Beverage *beverage)  {
    pBevrage = beverage;
}

std::string Mocha::getDescription() const {
    std::string desc = pBevrage ? pBevrage->getDescription() : "Unknown";
    return desc + ", Mocha";
}

double Mocha::cost() {
    auto co = pBevrage ? pBevrage->cost() : 0.0;
    return 0.2 + co;
}