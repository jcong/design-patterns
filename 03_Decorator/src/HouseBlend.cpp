#include "HouseBlend.h"

std::string HouseBlend::getDescription() const {
    return "House Blend Coffee";
}

double HouseBlend::cost() {
    return 0.89;
}