#ifndef __HOUSE_BLEND_H__
#define __HOUSE_BLEND_H__

#include "Beverage.h"

#include <string>

class HouseBlend : public Beverage {
public:
    virtual std::string getDescription() const final;
    virtual double cost() final;
};

#endif // __HOUSE_BLEND_H__