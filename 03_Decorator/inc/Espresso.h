#ifndef __ESPRESSO_H__
#define __ESPRESSO_H__

#include "Beverage.h"

#include <string>

class Espresso : public Beverage {
public:
    virtual std::string getDescription() const final;
    virtual double cost() final;
};

#endif // __ESPRESSO_H__