#ifndef __CONDIMENT_DECORATOR_H__
#define __CONDIMENT_DECORATOR_H__

#include "Beverage.h"

#include <string>

class CondimentDecorator : public Beverage {
public:
    virtual std::string getDescription() const = 0;
    virtual double cost() = 0;
};

#endif //  __CONDIMENR_DECORATOR_H__