#ifndef __BEVERAGE_H__
#define __BEVERAGE_H__

#include <string>

// interface
class Beverage {
public:
    virtual std::string getDescription() const = 0;
    virtual double cost() = 0;
};

#endif // __BEVERAGE_H__