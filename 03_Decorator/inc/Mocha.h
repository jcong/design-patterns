#ifndef __MOCHA_H__
#define __MOCHA_H__

#include "Beverage.h"
#include "CondimentDecorator.h"

#include <string>

class Mocha : public CondimentDecorator {
public:
    explicit Mocha(Beverage *beverage);
    ~Mocha() = default;

    virtual std::string getDescription() const final;
    virtual double cost() final;

private:
    Beverage *pBevrage;
};

#endif // __MOCHA_H__