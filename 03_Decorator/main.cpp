#include "Beverage.h"
#include "CondimentDecorator.h"

#include "Espresso.h"
#include "HouseBlend.h"
#include "Mocha.h"

#include <iostream>

void displayInfo(Beverage *beverage) {
    std::cout << "You have a  cup of : " << beverage->getDescription() << std::endl;
    std::cout << "The price is : " << beverage->cost() << std::endl; 
}

int main() {
    std::cout << "Make a cup of espresso" << std::endl;
    Espresso coffee;
    displayInfo(&coffee);
    std::cout << "Add some mocha" << std::endl;
    auto coffeeMocha = Mocha(&coffee);
    displayInfo(&coffeeMocha);
    std::cout << "Add more mocha" << std::endl;
    auto coffeeMochacha = Mocha(&coffeeMocha);
    displayInfo(&coffeeMochacha);
}