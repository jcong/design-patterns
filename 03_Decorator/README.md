**设计原则**

> **开闭原则**<br>
> 类应当对扩展开放，对修改关闭

**装饰者模式**

> 动态地将责任附加到对象之上。若要扩展功能，装饰者提供了比继承更有弹性的替代方案。

![装饰者模式UML](../UML/decorator.jpg)

```cpp
// 装饰者和被装饰对象有相同的超类类型
// 装饰者可以在所委托被装饰者的行为之前与/或之后，加上自己的行为，以达特定目的
```