**简单工厂模式**

> 又称为静态工厂方法。可以根据参数的不同动态地返回不同类的实例。工厂类中集中了各个具体类实例创建的逻辑，当具体类增多时，修改较大。违反了高内聚责任分配原则。它不属于23中设计模式之一。

![简单工厂模式UML](../UML/simple_factory.png)

```cpp
工厂角类： 核心、辅助创建具体类的实例，由外界调用。
抽象产品类： 所有具体产品类的父类，描述公共接口。
具体产品类： 工厂类创建的目标类。
```