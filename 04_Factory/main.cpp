#include "simple/PizzaStore.h"

#include <iostream>

int main() {
    std::cout << "----- simple factory pattern -----\n";
    PizzaStore store;
    store.orderPizza(PEPPERONI_PIZZA);
    store.orderPizza(VEGGLE_PIZZA);
    std::cout << "------- end simple factory -------\n\n";
}