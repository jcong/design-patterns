#ifndef __SIMPLE_PIZZA_FACTORY_H__
#define __SIMPLE_PIZZA_FACTORY_H__

#include "simple/Pizza.h"
#include "simple/CheesePizza.h"
#include "simple/VegglePizza.h"
#include "simple/ClamPizza.h"
#include "simple/PepperoniPizza.h"

#include <cstdint>

enum PizzaType : uint32_t {
    CHEESE_PIZZA,
    VEGGLE_PIZZA,
    CLAM_PIZZA,
    PEPPERONI_PIZZA,
};

class SimplePizzaFactory {
public:
    virtual Pizza *createPizza(PizzaType type) {
        switch (type)
        {
            case CHEESE_PIZZA:
                return new CheesePizza("cheese pizza");
            case VEGGLE_PIZZA:
                return new VegglePizza("veggle pizza");
            case CLAM_PIZZA:
                return new ClamPizza("clam pizza");
            case PEPPERONI_PIZZA:
                return new PepperoniPizza("pepperoni pizza");
            default:
                return nullptr;
        }
    }
};

#endif // __SIMPLE_PIZZA_FACTORY_H__