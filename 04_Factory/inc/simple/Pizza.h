#ifndef __PIZZA_H__
#define __PIZZA_H__

#include <string>
#include <iostream>

class Pizza {
public:
    explicit Pizza(std::string name = "Unknown") : pizzaName(name) {}

    virtual void prepare() {
        std::cout << pizzaName << " is preparing..." << std::endl;
    }

    virtual void bake() {
        std::cout << pizzaName << " is baking..." << std::endl;
    }

    virtual void cut() {
        std::cout << pizzaName << " is cutting..." << std::endl;
    }

    virtual void box() {
        std::cout << pizzaName << " is boxing..." << std::endl;
    }

    std::string pizzaName;
};

#endif // __PIZZA_H__