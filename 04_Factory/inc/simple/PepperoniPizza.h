#ifndef __PEPPERONI_PIZZA_H__
#define __PEPPERONI_PIZZA_H__

#include "simple/Pizza.h"

#include <string>

class PepperoniPizza : public Pizza {
public:
    explicit PepperoniPizza(std::string name);
};

#endif // __PEPPERONI_PIZZA_H__