#ifndef __VEGGLE_PIZZA_H__
#define __VEGGLE_PIZZA_H__

#include "simple/Pizza.h"

#include <string>

class VegglePizza : public Pizza {
public:
    explicit VegglePizza(std::string name);
};

#endif // __VEGGLE_PIZZA_H__