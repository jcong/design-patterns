#ifndef __CLAM_PIZZA_H__
#define __CLAM_PIZZA_H__

#include "simple/Pizza.h"

#include <string>

class ClamPizza : public Pizza {
public:
    explicit ClamPizza(std::string name);
};

#endif // __CLAM_PIZZA_H__