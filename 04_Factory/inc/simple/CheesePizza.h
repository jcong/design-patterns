#ifndef __CHEESE_PIZZA_H__
#define __CHEESE_PIZZA_H__

#include "simple/Pizza.h"

#include <string>

class CheesePizza : public Pizza {
public:
    explicit CheesePizza(std::string name);
};

#endif // __CHEESE_PIZZA_H__