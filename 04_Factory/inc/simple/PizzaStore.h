#ifndef __PIZZA_STORE_H__
#define __PIZZA_STORE_H__

#include "simple/SimplePizzaFactory.h"

class PizzaStore {
public:
    virtual void orderPizza(PizzaType type) {
        SimplePizzaFactory factory;
        auto *pizza = factory.createPizza(type);
        pizza->prepare();
        pizza->bake();
        pizza->cut();
        pizza->box();
        delete pizza;
    }
};

#endif // __PIZZA_STORE_H__