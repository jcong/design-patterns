#ifndef __STATISTICS_DISPLAY_H__
#define __STATISTICS_DISPLAY_H__

#include "Observer.h"
#include "DisplayElement.h"
#include "Subject.h"

#include <vector>

class StatisticsDisplay : public Observer, DisplayElement {
public:
    StatisticsDisplay(Subject *weatherData);
    ~StatisticsDisplay() = default;

    virtual void update(float, float, float) final;
    virtual void display() final;
    virtual const char *getName() const final;
    virtual void detach() final;

private:
    void updateAverage();

    std::vector<float> temperatures;
    std::vector<float> humidities;
    std::vector<float> pressures;

    float averageTemperature;
    float averageHumidity;
    float averagePressure;

    Subject *pImplWeatherData;
};

#endif // __STATISTICS_DISPLAY_H__