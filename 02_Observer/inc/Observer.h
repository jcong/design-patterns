#ifndef __OBSERVER_H__
#define __OBSERVER_H__

// interface
class Observer {
public:
    virtual void update(float temp, float humidity, float pressure) = 0;
    virtual const char *getName() const = 0;
    virtual void detach() = 0;

};

#endif // __OBSERVER_H__