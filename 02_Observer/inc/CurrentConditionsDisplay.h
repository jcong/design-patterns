#ifndef __CURRENT_CONDITIONS_DISPLAY_H__
#define __CURRENT_CONDITIONS_DISPLAY_H__

#include "Observer.h"
#include "DisplayElement.h"
#include "Subject.h"

class CurrentConditionsDisplay : public Observer, DisplayElement {
public:
    CurrentConditionsDisplay(Subject *weatherData);
    ~CurrentConditionsDisplay() = default;

    virtual void update(float, float, float) final;
    virtual void display() final;
    virtual const char *getName() const final;
    virtual void detach() final;

private:
    float temperature;
    float humidity;
    Subject *pImplWeatherData;
};

#endif // __CURRENT_CONDITIONS_DISPLAY_H__