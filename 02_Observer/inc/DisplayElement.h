#ifndef __DISPLAY_ELEMENT_H__
#define __DISPLAY_ELEMENT_H__

// interface
class DisplayElement {
public:
    virtual void display() = 0;
};

#endif // __DISPLAY_ELEMENT_H__