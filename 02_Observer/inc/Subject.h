#ifndef __SUBJECT_H__
#define __SUBJECT_H__

#include "Observer.h"

// interface
class Subject {
public:
    virtual void registerObserver(Observer *) = 0;
    virtual void removeObserver(Observer *) = 0;
    virtual void notifyObserver() = 0;
};

#endif // __SUBJECT_H__