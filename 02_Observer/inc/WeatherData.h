#ifndef __WEATHER_DATA_H__
#define __WEATHER_DATA_H__

#include "Subject.h"
#include "Observer.h"

#include <list>

class WeatherData : public Subject {
public:
    virtual void registerObserver(Observer *observer) final;
    virtual void removeObserver(Observer *observer) final;
    virtual void notifyObserver() final;

    void measurementsChanged();
    void setMeasurements(float temperature, float humidity, float pressure);
    
private:
    std::list<Observer *> observers;
    float temperature;
    float humidity;
    float pressure;
};

#endif // __WEATHER_DATA_H__