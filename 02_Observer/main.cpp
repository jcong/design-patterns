#include "CurrentConditionsDisplay.h"
#include "StatisticsDisplay.h"
#include "WeatherData.h"

#include <iostream>

int main() {
    WeatherData weatherData;

    CurrentConditionsDisplay currentDisplay(&weatherData);
    StatisticsDisplay statisticsDisplay(&weatherData);

    weatherData.setMeasurements(80.0f, 65.0f, 30.4f);
    std::cout << ">> remove statistics display <<" << std::endl;
    statisticsDisplay.detach();
    weatherData.setMeasurements(82.0f, 70.0f, 29.2f);
    std::cout << ">> remove current conditions display <<" << std::endl;
    currentDisplay.detach();
    weatherData.setMeasurements(78.0f, 90.0f, 29.2f);
}