#include "CurrentConditionsDisplay.h"
#include "WeatherData.h"

#include <iostream>

CurrentConditionsDisplay::CurrentConditionsDisplay(Subject *weatherData) {
    pImplWeatherData = weatherData;
    pImplWeatherData->registerObserver(this);
}

void CurrentConditionsDisplay::update(float temperature, float humidity, float pressure) {
    this->temperature = temperature;
    this->humidity = humidity;
    display();
}

void CurrentConditionsDisplay::display() {
    std::cout << "Current conditions: " << std::endl;
    std::cout << "\ttemperature: " << temperature << "F degrees." << std::endl;
    std::cout << "\thumidity   : " << humidity << "% humidity." << std::endl;
}

void CurrentConditionsDisplay::detach() {
    pImplWeatherData->removeObserver(this);
}

const char *CurrentConditionsDisplay::getName() const {
    return "current conditions display";
}