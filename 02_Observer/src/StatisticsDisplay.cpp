#include "StatisticsDisplay.h"
#include "WeatherData.h"

#include <iostream>

StatisticsDisplay::StatisticsDisplay(Subject *weatherData) {
    pImplWeatherData = weatherData;
    pImplWeatherData->registerObserver(this);
}

void StatisticsDisplay::update(float temperature, float humidity, float pressure) {
    temperatures.emplace_back(temperature);
    humidities.emplace_back(humidity);
    pressures.emplace_back(pressure);

    updateAverage();
    display();
}

void StatisticsDisplay::display() {
    std::cout << "Statistics:" << std::endl;
    std::cout << "\taverage temperature: " << averagePressure << "F degrees." << std::endl;
    std::cout << "\taverage humidity   : " << averageHumidity << "% humidity." << std::endl;
    std::cout << "\taverage pressure   : " << averagePressure << "kPa." << std::endl;
}

const char *StatisticsDisplay::getName() const {
    return "Statistics Display";
}

void StatisticsDisplay::detach() {
    pImplWeatherData->removeObserver(this);
}

void StatisticsDisplay::updateAverage() {
    auto updateAve = [](const std::vector<float> &vec, float &ave) {
        if (vec.empty()) {
            ave = 0.0;
            return;
        }
        auto size = vec.size();
        float sum = 0.0;
        for (const auto &e : vec) sum += e;
        ave = sum / static_cast<float>(size);
    };

    updateAve(temperatures, averageTemperature);
    updateAve(humidities, averageHumidity);
    updateAve(pressures, averagePressure);
}