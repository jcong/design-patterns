#include "WeatherData.h"

#include <iostream>

void WeatherData::registerObserver(Observer *observer) {
    observers.emplace_back(observer);
    std::cout << observer->getName() << " added into observers" << std::endl;
    std::cout << "Obsevers count : " << observers.size() << std::endl;
}

void WeatherData::removeObserver(Observer *observer) {
    observers.remove(observer);
    std::cout << observer->getName() << " removed from obsetvers" << std::endl;
    std::cout << "Obsevers count : " << observers.size() << std::endl;
}

void WeatherData::notifyObserver() {
    for (auto &o : observers) {
        o->update(temperature, humidity, pressure);
    }
}

void WeatherData::measurementsChanged() {
    notifyObserver();
}

void WeatherData::setMeasurements(float temperature, float humidity, float pressure) {
    std::cout << "Setting measurements, will notify to observers..." << std::endl;
    this->temperature = temperature;
    this->humidity = humidity;
    this->pressure = pressure;
    measurementsChanged();
}